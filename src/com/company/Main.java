package com.company;

public class Main {

    public static void main(String[] args) {
        /* If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
        Find the sum of all the multiples of 3 or 5 below 1000. */

        Integer natBelow [] = new Integer[1000];
        Integer sumAll = 0;

        for (int i = 0; i < 999; i++) {
            if (((i+1) % 3 == 0) || ((i+1) % 5 == 0)){
                natBelow[i] = i+1 ;
                sumAll = sumAll + natBelow[i];
            }
        }



        for (int i = 0; i < natBelow.length; i++) {
            System.out.println(natBelow[i]);
        }
        System.out.println(sumAll);

        //  233168


    }
}
